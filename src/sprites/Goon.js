import Enemy from './Enemy.js';

export default class Goon extends Enemy {
  constructor(config) {
    super(config);
    this.body.setVelocity(0, 0).setBounce(0.2).setCollideWorldBounds(true);
  }

  preload() {}

  create() {}

  destroy() {}
}
