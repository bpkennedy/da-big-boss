class BootScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'BootScene'
    });
  }

  preload() {
    this.load.multiatlas('person', 'assets/person.json', 'assets');
  }

  create() {
    this.scene.start('TitleScene');
  }
}

export default BootScene;
