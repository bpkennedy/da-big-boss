import Goon from '../sprites/Goon';

class GameScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'GameScene'
    });
    this.cursors;
    this.goon;
  }

  preload() {
    this.goon = new Goon({
      scene: this,
      x: this.sys.game.config.width / 2,
      y: this.sys.game.config.height / 2,
      key: 'person'
    });
  }

  create() {
    this.cursors = this.input.keyboard.createCursorKeys();
  }

  update() {
    if (this.cursors.left.isDown) {
      this.goon.body.setVelocityX(-50);
    } else if (this.cursors.right.isDown) {
      this.goon.body.setVelocityX(50);
    } else {
      this.goon.body.setVelocityX(0);
    }
  }

}
export default GameScene;
