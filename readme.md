Da Big Boss
============
Play as Da Big Boss, notorious overlord/orchestrator of the St. Louis crime syndicate T.U.R.D. whose sole purpose is to prevent the city's hero, Spruce Knee, from rescuing his damsel in distress, Jane Austere. You operate behind the scenes, sending your villainous henchmen out against the Hero in a no-holds-barred attempt to bring him down!

> A game for the browser, built using the [Phaser](https://phaser.io/) game framework.

### Install
* `git clone git@gitlab.com:bpkennedy/da-big-boss.git`
* `cd da-big-man && npm install`

### Develop
* `npm run dev` - webpack compiles `/src` files and serves on localhost:8080, with `webpack-dev-server` watching changes to them and recompiling the bundle on each change.
* `npm run lint` - runs eslint on `/src` files.  This is a modified version (loosened) of the .eslint settings for the [Phaser project](https://github.com/photonstorm/phaser/blob/master/.eslintrc.json).
